<?php
    require "verificationConnexion.php";
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title> Boutique Classique_Web </title>
        <link href="../css/bootstrap-theme.css" rel="stylesheet" type="text/css">
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../css/style.css" rel="stylesheet" type="text/css"> 
        <script src="../js/jquery.js"></script>
        <script src="../js/bootstrap.min.js"></script>
    </head>

    <body>

        <!-- MENU -->
        <?php
           include("menu.php");
        ?>

        <div class="jumbotron">
            <h1> Bienvenue  ! </h1>
			<p> Ce site e-commerce vous permet de faire des achats d'album de musique contenu dans la base
				de données Classique_Web.
			</p>
			<p> Pour cela, il suffit de cliquer sur l'onglet boutique pour choisir vos albums, et de vous
				connecter pour les ajouter à votre panier.
			</p>
        </div>

    </body>
</html>