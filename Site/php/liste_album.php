<?php
    require "verificationConnexion.php";
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title> Boutique Classique_Web </title>
        <link href="../css/bootstrap-theme.css" rel="stylesheet" type="text/css">
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../css/style.css" rel="stylesheet" type="text/css" media ="all" />
        <script src="../js/jquery.js"></script>
        <script src="../js/bootstrap.min.js"></script>
    </head>
    <body>
         <!-- MENU --> 
        <?php

            include "menu.php";

            require "connexionBD.php";

            // Tous les albums du musicien
            $request="SELECT DISTINCT Album.Code_Album,Titre_Album,Année_Album as annee
                  FROM Album
                  join Disque on Disque.Code_Album=Album.Code_Album
                  join Composition_Disque on Composition_Disque.Code_Disque=Disque.Code_Disque
                  join Enregistrement on Enregistrement.Code_Morceau=Composition_Disque.Code_Morceau
                  join Composition on Composition.Code_Composition=Enregistrement.Code_Composition
                  join Composition_Oeuvre on Composition.Code_Composition=Composition_Oeuvre.Code_Composition
                  join Oeuvre on Oeuvre.Code_Oeuvre=Composition_Oeuvre.Code_Oeuvre
                  join Composer on Composer.Code_Oeuvre=Oeuvre.Code_Oeuvre
                  join Musicien on Composer.Code_Musicien=Musicien.Code_Musicien
                  where Composer.Code_Musicien=?";

            $request2="SELECT Titre,Enregistrement.Code_Morceau
                       FROM Enregistrement
                       join Composition on composition.Code_Composition=Enregistrement.Code_Composition
                       join Composition_Oeuvre on Composition_Oeuvre.Code_Composition=Composition.Code_Composition
                       join Oeuvre on oeuvre.Code_Oeuvre=Composition_Oeuvre.Code_Oeuvre
                       join Composer on composer.Code_Oeuvre=Oeuvre.Code_Oeuvre
                       join Musicien on Composer.Code_Musicien=Musicien.Code_Musicien
                       WHERE Musicien.Code_Musicien=?";

            $query = $pdo->prepare($request);
            $query->execute(array($_GET['Code']));

            $query2=$pdo->prepare($request2);
            $query2->execute(array($_GET['Code']));
            
            // S'il y a des résultats, affiche l'image du musicien
            if($album=$query->fetch())
            {
                $lien="image.php?Code=".$_GET['Code'];

                echo "<img src='$lien' width=200 id='image' alt='Image compositeur'/>".$_GET['Nom']." ".$_GET['prenom']."<br>";//photo du musicien
                echo "<div>";

                // Affiche toutes les pochettes de ses albums
                do {
                    $lien_album="pochette.php?Code=".$album['Code_Album'];
                    $lien_enregistrements = "enregistrement.php?Code=".$album['Code_Album']."&Album=".$album['Titre_Album'];

                    echo "<a href='$lien_enregistrements'>";
                    echo "<img src='$lien_album' width=200 alt='Pochette album' />";//id='pochette'
                    echo " </a>";
                    echo $album['Titre_Album']." (".$album['annee'].")";
                    $lien_ajout_panier = "traiteAjoutPanier.php?Code=".$album['Code_Album'];
                    echo "<form method='post' action='$lien_ajout_panier'>
                                    <input type ='submit' value='Ajouter au panier'/>
                                  </form>"."<br><br>";

                }
                while($album=$query->fetch());
                echo "</div>";
            }
            // S'il n'y a pas de résultats, afficher ce message
            else {
                echo "Aucun album"."<br>";
                if($enregistrement=$query2->fetch())
                {
                    do
                    {
                        // Titre morceau
                        echo $enregistrement['Titre']."<br><br>";
                        // Ecoute du morceau
                        echo "<audio src='/Classique/Home/Extrait/".$enregistrement['Code_Morceau'];
                        echo "' controls>Erreur</audio>"."<br><br>";
                    }
                    while($enregistrement =$query2->fetch());
                }
                else
                    echo " Et aucun enregistrement" . "<br>";
            }
            $pdo = null;
        ?>

    </body>
</html>






















