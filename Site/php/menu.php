
<?php
    echo ("
            <nav class='navbar navbar-inverse navbar-fixed-top' role='navigation'>
                <div class='container'>
                    <!-- Home -->
                    <a class='navbar-brand' href='index.php' >
                        <i class='glyphicon glyphicon-home'></i> Home
                    </a>

                    <!-- Boutique + A propos -->
                   <div>
                        <ul class='nav navbar-nav'>
                            <li>
                                <a href='boutique.php'>Boutique</a>
                            </li>
                            <li>
                                <a href='apropos.php'>A propos</a>
                            </li>
                        </ul>
                        ");

                       // Connexion / Deconnexion
                        if (isset($_SESSION["NOM_USER"])) {
                            echo("
                                <ul class='nav navbar-nav navbar-right'>
                                    <li>
                                        <a href='#' style='font-size : 10px;'> Vous êtes connecté en tant que $user_login </a>
                                    </li>
                                    <li>
                                        <a href='panier.php'> Mon panier </a>
                                    </li>
                                    <li>
                                        <a href='deconnexion.php'> Déconnexion </a>
                                    </li>
                                </ul>
                            ");
                        }
                        else {
                            echo("
                            <ul class='nav navbar-nav navbar-right'>
                                <li>
                                    <a href = 'connexion.php' > Connexion </a >
                                </li>
                                <li>
                                    <a href = 'inscription.php' > Inscription </a >
                                </li>
                            </ul>
                            ");
                        }
    echo("
                    </div>
            <!-- /.navbar-collapse -->
                </div>
            <!-- /.container -->
            </nav>
        ");
?>