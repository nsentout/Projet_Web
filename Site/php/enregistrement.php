<?php
	require "verificationConnexion.php";
?>

<!DOCTYPE html>
<html>
	<head>
	    <meta charset="utf-8"/>
	    <title> Boutique Classique_Web </title>
	    <link href="../css/bootstrap-theme.css" rel="stylesheet" type="text/css">
	    <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css">
	    <script src="../js/jquery.js"></script>
	    <script src="../js/bootstrap.min.js"></script>
	</head>
	<body>

		<?php
			// MENU //
			include "menu.php";

			require "connexionBD.php";

			$lien = basename($_SERVER['HTTP_REFERER']);
			echo "<a href='$lien'> Retour </a> <br><br>";

			// Code album
			$code_album = $_GET['Code'];

			// Tous les titres de l'album
			$request ="SELECT Titre, Enregistrement.Code_Morceau FROM Album
						join Disque on Disque.Code_Album = Album.Code_Album
						join Composition_Disque on Composition_Disque.Code_Disque = Disque.Code_Disque
						join Enregistrement on Enregistrement.Code_Morceau = Composition_Disque.Code_Morceau
						WHERE Album.Code_Album=?";

			$query = $pdo->prepare($request);
			$query->execute(array($code_album));

				////////////
				// AMAZON //
				////////////
			require "accesAmazon.php";
			
			$requete_asin = "SELECT ASIN FROM Album
							  WHERE Album.Code_Album=$code_album";

			$result = $pdo->query($requete_asin);

			foreach ($result as $row) {
				$asin = $row[0];
			}

			$response = $client->responseGroup('Large')->lookup($asin);

			if (sizeof($response['Items']) > 1)
			{
				$lien_achat = $response['Items']['Item']['DetailPageURL'];
				$prix = $response['Items']['Item']['OfferSummary']['LowestNewPrice']['FormattedPrice'];

				// Bouton achat Amazon
				echo " <a class='btn btn-primary btn-lg' role='button' href='$lien_achat'>
							Acheter l'album sur Amazon (".$prix.")
					   </a><br><br>";
				// Bouton ajout panier
				$lien_ajout_panier = "traiteAjoutPanier.php?Code=".$code_album;
	            echo "<form method='post' action='$lien_ajout_panier'>
	                  		<input type ='submit' value='Ajouter au panier'/>
	                  </form>"."<br><br>";
			}
			else
				echo "Pas de lien vers Amazon existant";
			
			/////////////////////////////////////////////////////////////
			
			// Si l'album contient au moins 1 enregistrement, les afficher + permettre des les écouter
			if($albums= $query->fetch())
			{
				echo "<h4>Morceaux de l'album ".$_GET['Album']." : "."</h4><br>";
				do
				{
					// Titre morceau
					echo $albums['Titre']."<br><br>";
					// Ecoute du morceau
					echo "<audio src='/Classique/Home/Extrait/".$albums['Code_Morceau'];
					echo "' controls>Erreur</audio>"."<br><br><br>";
				}
				while($albums =$query->fetch());
			}
			// Sinon, afficher ce message
			else
				echo "Aucun enregistrement". "<br>";

			$pdo = null;
		?>

	</body>
</html>


    


