<?php
	require "connexionBD.php";

	// Affiche l'image du musicien //
    $stmt = $pdo->prepare("SELECT Photo FROM Musicien WHERE Code_Musicien=?");
    $stmt->execute(array($_GET['Code']));
    $stmt->bindColumn(1, $lob, PDO::PARAM_LOB);
    $stmt->fetch(PDO::FETCH_BOUND);
    $image = pack("H*", $lob);
    header("Content-Type: image/jpeg");
    echo $image;
?>