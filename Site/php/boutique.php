<?php
    require "verificationConnexion.php";
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title> Boutique Classique_Web </title>
        <link href="../css/bootstrap-theme.css" rel="stylesheet" type="text/css">
        <link href="../css/style.css" rel="stylesheet" type="text/css">
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <script src="../js/jquery.js"></script>
        <script src="../js/bootstrap.min.js"></script>
    </head>
    <body>

        <!-- MENU -->
        <?php
            include("menu.php");
        ?>

        <!-- Barres de recherches -->
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Recherche par compositeurs</h2>
                    <div id="custom-search-input">
                        <div class="input-group col-md-8">
                            <form method="post" action="boutique.php">
                                <input name="initiale_compositeur" type="text" class="form-control input-lg" placeholder="Rechercher"/>
                            </form>
                            <span class="input-group-btn">
                                <button class="btn btn-info btn-lg" type="button">
                                   <i class="glyphicon glyphicon-search"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
                 <div class="col-md-6" style="margin-bottom : 50px;">
                    <h2>Recherche par albums</h2>
                    <div id="custom-search-input">
                        <div class="input-group col-md-8">
                            <form method="post" action="boutique.php">
                                <input name="initiale_album" type="text" class="form-control input-lg" placeholder="Rechercher"/>
                            </form>
                            <span class="input-group-btn">
                                <button class="btn btn-info btn-lg" type="button">
                                    <i class="glyphicon glyphicon-search"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
           
           require "connexionBD.php";


            // Si une recherche par compositeurs est lancée 
            if (isset($_POST['initiale_compositeur'])) 
            {
                $initiale_compositeur = $_POST["initiale_compositeur"];
                // Tous les compositeurs dont le nom commence par $initiale_compositeur
                $requete_compositeur = "SELECT DISTINCT Nom_Musicien,Prénom_Musicien as prenom, Musicien.Code_Musicien
                                        FROM Musicien
                                        join Composer on Composer.Code_Musicien=Musicien.code_Musicien
                                        join Oeuvre on Oeuvre.Code_Oeuvre=Composer.Code_Oeuvre
                                        WHERE Nom_Musicien Like '$initiale_compositeur%' Order by Nom_Musicien";
                $result = $pdo->query($requete_compositeur);

                // S'il n'y a pas de résultat, afficher ce message
                if ($result->rowCount() == 0)
                    echo "Aucun résultat";

                // Sinon, afficher tous les compositeurs avec un lien vers leurs albums
                else {               
                    foreach ($result as $row_musicien) {
                        
                        // Image avec lien vers ses albums
                        $lien_albums = "liste_album.php?Code=".$row_musicien['Code_Musicien']."&Nom=".$row_musicien['Nom_Musicien']."&prenom=".$row_musicien['prenom'];
                        $photo = "image.php?Code=".$row_musicien['Code_Musicien'];                         
                       
                        echo "<a href='$lien_albums'>";                   
                        echo "<img src='$photo' class='photo' />";    
                        echo "</a>";   
                     
                        // Nom musicien
                        echo $row_musicien['Nom_Musicien']." ".$row_musicien['prenom']."<br>";
                        echo "<br>";
                    }                   
                }

                $pdo = null;
            }

            // Si une recherche par album est lancée 
            else if (isset($_POST['initiale_album'])) 
            {
                $initiale_album = $_POST["initiale_album"];
                // Tous les albums dont le nom compmence par $initiale_album
                $requete_album = "SELECT Titre_Album,Année_Album as annee, Album.Code_Album FROM Album
                                   WHERE Titre_Album Like '$initiale_album%' Order by Titre_Album";
                                   
                $result = $pdo->query($requete_album);

                if ($result->rowCount() == 0)
                    echo "Aucun résultat";
                
                else
                    foreach ($result as $row_album) {
                        // Image album avec lien vers ses morceaux
                        $lien_enregistrements = "enregistrement.php?Code=".$row_album['Code_Album']
                            ."&Album=".$row_album['Titre_Album'];
                        echo "<a href='$lien_enregistrements'>";
                        $pochette="pochette.php?Code=".$row_album['Code_Album'];
                        echo "<img src='$pochette' width=200 heigth=200' />";
                        echo "</a>";
                        // Titre album
                        echo $row_album['Titre_Album']." (".$row_album['annee'].")";
                        // Bouton ajout panier
                        $lien_ajout_panier = "traiteAjoutPanier.php?Code=".$row_album['Code_Album'];
                        echo "<form method='post' action='$lien_ajout_panier'>
                                <input type ='submit' value='Ajouter au panier'/>
                              </form>"."<br><br>";
                    }         
                $pdo = null;
            }
            
        ?>

    </body>
</html>