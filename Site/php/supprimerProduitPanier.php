<DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title> Boutique Classique_Web </title>
        <link href="../css/bootstrap-theme.css" rel="stylesheet" type="text/css">
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../css/style.css" rel="stylesheet" type="text/css" media ="all" />
        <script src="../js/jquery.js"></script>
        <script src="../js/bootstrap.min.js"></script>
    </head>
    <body>
		
    	<?php
    	
			require "verificationConnexion.php";
                        
            // Code album
            $code_album = $_GET['Code'];
		
			require "connexionBD.php";

			// Suppression de l'album du panier
			$requete_suppression_produit = "DELETE Achat
											FROM Abonné
											join Achat on Achat.Code_Abonné = Abonné.Code_Abonné
											join Enregistrement on Enregistrement.Code_Morceau = Achat.Code_Enregistrement
											join Composition_Disque on Composition_Disque.Code_Morceau = Enregistrement.Code_Morceau
											join Disque on Disque.Code_Disque = Composition_Disque.Code_Disque
											join Album on Album.Code_Album = Disque.Code_Album
											WHERE Login='$user_login' and Album.Code_Album = '$code_album'";

			$pdo->query($requete_suppression_produit);
					
			$pdo = null;
			
			// Redirection vers le panier
			header("Location: panier.php");
												
		?>
   </body>
</html>
