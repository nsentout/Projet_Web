<?php
    require "verificationConnexion.php";
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<title> Boutique Classique_Web </title>
        <link href="../css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/style.css" rel="stylesheet" type="text/css" media ="all" />
		<script src="../js/jquery.js"></script>
		<script src="../js/bootstrap.min.js"></script>
	</head>
	<body>

        <!-- MENU -->
        <?php
            include "menu.php";
        ?>

        <!-- Formulaire de connexion -->

		<div class ="col-md-3">
			<form method="post" action="traiteConnexion.php">
                <?php if(isset($_GET["connex"])) { ?> <!--si le mot de passe ou login est faux -->
                    Identifiant
                    <div class="input-group">
                        <input type="text" name="Login" class="form-control" placeholder="Identifiant" id="echec">
                    </div><br>
                    Mot de passe
                    <div class="input-group">
                        <input type="password" name="Password" class="form-control" placeholder="Mot de passe"  id="echecMp">
                    </div><br>
                    <p><i> Mot de passe ou identifiant invalide</i></p>
                <?php } else { ?>
                    Identifiant
                    <div class="input-group">
                        <input type="text" name="Login" class="form-control" placeholder="Identifiant">
                    </div><br>
                    Mot de passe
                    <div class="input-group">
                        <input type="password" name="Password" class="form-control" placeholder="Mot de passe">
                    </div><br>
                <?php } ?>
				<input name="Connect" type="submit" value="Se connecter" />
			</form>
		</div>

	</body>
</html>