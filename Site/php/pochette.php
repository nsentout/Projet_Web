<?php
	require "connexionBD.php";

	// Affiche la pochette de l'album
	$stmt = $pdo->prepare("SELECT Pochette FROM Album WHERE Code_Album=?");
	$stmt->execute(array($_GET['Code']));
	$stmt->bindColumn(1, $lob, PDO::PARAM_LOB);
	$stmt->fetch(PDO::FETCH_BOUND);
	$image = pack("H*", $lob);
	header("Content-Type: image/jpeg");
	echo $image;
?>