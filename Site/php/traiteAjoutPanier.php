<?php
    require "verificationConnexion.php";
?>


<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title> Boutique Classique_Web </title>
        <link href="../css/bootstrap-theme.css" rel="stylesheet" type="text/css">
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <script src="../js/jquery.js"></script>
        <script src="../js/bootstrap.min.js"></script>
    </head>
    <body>

    	<?php

			include "menu.php";

			// Si l'utilisateur est connecté
		    if (isset($_SESSION["NOM_USER"]))
		    {
				// Code album
				$code_album = $_GET['Code'];

				// Connexion à la BD
				require "connexionBD.php";
			  

			    ///// Code de l'utilisateur connecté /////
				$requete_code_user = "SELECT Code_Abonné FROM Abonné
										WHERE Login='$user_login' ";

					// Execution de la requete
			    $result = $pdo->query($requete_code_user);

			    foreach ($result as $row) {
			    	$code_abonné = $row[0];	
	            }

				///// Titre album acheté /////
				$requete_titre_album_achete = "SELECT Titre_Album FROM Album
												WHERE Code_Album='$code_album'";

					// Execution de la requete
			    $result = $pdo->query($requete_titre_album_achete);

			    foreach ($result as $row) {
			    	$titre_album_achete = $row[0];   
	            }

	            ///// Tous les morceaux inclus dans l'album acheté /////
				$requete_morceaux_achetes = "SELECT Enregistrement.Code_Morceau, Titre FROM Enregistrement
											join Composition_Disque on Composition_Disque.Code_Morceau = Enregistrement.Code_Morceau
											join Disque on Disque.Code_Disque = Composition_Disque.Code_Disque
											join Album on Album.Code_Album = Disque.Code_Album
											WHERE Album.Code_Album='$code_album'";

					// Execution de la requete
			    $result = $pdo->query($requete_morceaux_achetes);

			    // S'il n'y a pas d'enregistrements
				if ($result->rowCount() == 0)
				{
						$pdo = null;
						header('Refresh: 1; url=boutique.php'); 
						echo "Vous ne pouvez pas ajouter d'album sans enregistrement à votre panier.<br>";
				}

				else
				{
				    $i = 0;
				    $morceaux = array();	// contient tous les morceaux de l'album acheté
				    foreach ($result as $row) {
		                $morceaux[$i] = $row['Code_Morceau'];
		                $i++;
		            }

		            ///// Achat de tous les morceaux de l'album /////
		            $requete_achat = "INSERT INTO Achat
									  VALUES(:code_morceau_achete,$code_abonné)";

						// Execution de la requete
					$query = $pdo->prepare($requete_achat);

					for ($i=0; $i<sizeof($morceaux); $i++)
		            {
	       				$query->execute(array(':code_morceau_achete' => $morceaux[$i]));
	       			}

	       			$pdo = null;
				
					// Redirection vers le panier
				    header("Location :panier.php");
	       		}
       			
       			
			}

			else
			{
				// Redirection vers la page de connexion		
				echo "Vous devez vous connecter pour ajouter un album au panier !<br>";	
				header('Refresh: 1; url=connexion.php'); 	
			}
		?>

    </body>
</html>

