<?php
    session_start();

    // Vérifie si l'utilisateur est connecté
    if (isset($_SESSION["NOM_USER"]))
    {
        if (time() > $_SESSION['expire'])
            session_destroy();
        $user_login = $_SESSION["NOM_USER"];
    }

?>