<?php
    require "verificationConnexion.php";
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title> Boutique Classique_Web </title>
        <link href="../css/bootstrap-theme.css" rel="stylesheet" type="text/css">
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <script src="../js/jquery.js"></script>
        <script src="../js/bootstrap.min.js"></script>
    </head>
    <body>

        <?php
        	// MENU //
            include "menu.php";
            
            // Si l'utilisateur est connecté, afficher son panier
            if (isset($_SESSION["NOM_USER"]))
			{
				require "connexionBD.php";

				// Tous les achats de l'utilisateur
				$achats = "SELECT Titre_Album, Album.Code_Album, Album.Année_Album as annee, ASIN FROM Abonné
							join Achat on Achat.Code_Abonné = Abonné.Code_Abonné
							join Enregistrement on Enregistrement.Code_Morceau = Achat.Code_Enregistrement
							join Composition_Disque on Composition_Disque.Code_Morceau = Enregistrement.Code_Morceau
							join Disque on Disque.Code_Disque = Composition_Disque.Code_Disque
							join Album on Album.Code_Album = Disque.Code_Album
							WHERE Login='$user_login'
							ORDER BY Titre_Album, Album.Code_Album";

				$resultA = $pdo->query($achats);

				// Si le panier est  vide
				if ($resultA->rowCount() == 0)
						echo "Votre panier est vide.";

				// Sinon
				else {
					$tmp_album = "";  // Prendra la valeur du dernier album sélectionné

					/**  Tableau du panier **/
					echo " 
					<table class='table'>
				        <thead> 
				        	<tr> 
				        		<th scope=row>
									<h3> <span class='label label-default'>Album</span></h3>
								</th>
								<th scope=row>
									<h3> <span class='label label-default'>Morceaux contenus dans l'album</span></h3>
								</th> 
								<th scope=row>
									<h3> <span class='label label-default'>Détails</span></h3>
								</th> 
							</tr>
						</thead> 
						<tbody>";

					// Pour chaque album	
					foreach ($resultA as $rowA) {
						if ($tmp_album !=  $rowA['Titre_Album'])
						{
							/// Colonne Album ///
							$titre_album = $rowA['Titre_Album'];
							$lien_suppression_produit = "supprimerProduitPanier.php?Code=".$rowA['Code_Album'];
                            $lien_album="pochette.php?Code=".$rowA['Code_Album'];
							echo "<tr> 
									<td>
										<form method='post' action='$lien_suppression_produit'>
										    <img src='$lien_album' width='100' height='100' />
											<h4>".$rowA['Titre_Album']." (".$rowA['annee'].")"."</h4>
											<input type ='submit' value='Supprimer du panier'/>
										</form>
										<br><br>
								 ";	

							echo "	</td>
									<td>";
							$tmp_album = $rowA['Titre_Album'];

							/// Colonne morceaux ///
							// Tous les morceaux de cette album
							$morceaux = "SELECT Titre FROM Enregistrement
								  join Composition_Disque on Composition_Disque.Code_Morceau = Enregistrement.Code_Morceau
								  join Disque on Disque.Code_Disque = Composition_Disque.Code_Disque
								  join Album on Album.Code_Album = Disque.Code_Album 
								  WHERE Titre_Album='$titre_album'
								  GROUP BY Titre";

							$resultM = $pdo->query($morceaux);
							
							foreach($resultM as $rowM){
								echo $rowM['Titre']."<br>";
							}

							/// Colonne détails ///
							$code_album = $rowA['Code_Album'];
							// Le prix total de l'album
							$nb_albums = "SELECT Count(Prix) AS NbPrix FROM Enregistrement
								  join Composition_Disque on Composition_Disque.Code_Morceau = Enregistrement.Code_Morceau
								  join Disque on Disque.Code_Disque = Composition_Disque.Code_Disque
								  join Album on Album.Code_Album = Disque.Code_Album 
								  WHERE Titre_Album='$titre_album'";
								  
								  
							$resultP = $pdo->query($nb_albums);

							foreach($resultP as $rowP){
								echo "</td>
							  		  <td>
							  		  	<a href='enregistrement.php?Code=".$rowA['Code_Album']."&Album=".$rowA['Titre_Album']."'>
							  		  		Détails
							  		  	</a>
							  		  </td>
						 		 </tr>";
							}
						}					
					}

					echo "</tbody> 
					  </table> ";

					$pdo = null;
				}
			}
			// Si l'utilisateur n'est pas connecté, afficher ce message
			else
				echo "<div class='alert alert-danger' role='alert'>
						<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>
						<span class='sr-only'>Error:</span>
						Vous devez vous connecter pour afficher cette page !
					  </div>";
        ?>

    </body>
</html>
