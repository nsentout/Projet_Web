<?php
    require "verificationConnexion.php";
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<title> Boutique Classique_Web </title>
		<link href="../css/bootstrap-theme.css" rel="stylesheet" type="text/css">
		<link href="../css/style.css" rel="stylesheet" type="text/css">
		<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<script src="../js/jquery.js"></script>
		<script src="../js/bootstrap.min.js"></script>
	</head>

	<body>

        <?php
            include("menu.php");
        ?>

		<div class="jumbotron">
			<h3> Travail réalisé </h3>
			<ul>
				<li> Page d'accueil </li>
				<li> Menu facilitant la navigation </li>
				<li> Page à propos </li>
				<li> Boutique permettant de parcourir la base Classique_Web
					 avec 2 types de recherches 
					 <ul> 
					 	<li><i>Par compositeur</i> : la recherche affichera tous les compositeurs associés
					 		à celle-ci. En cliquant sur la photo du compositeur, vous aurez accès à ses albums, si celui-ci en a (sinon affiche les morceaux qu'il a réalisé ), 
					 		et aurez la possibilité de l'ajouter au panier si vous êtes connecté. A partir de la page 
					 		de ses albums, vous pouvez cliquer sur la pochette d'un album pour avoir accès à un lien vers 
					 		Amazon pour l'acheter ainsi qu'un extrait de tous les morceaux composant l'album
					    </li>
					 	<li><i>Par album</i> : la recherche affichera tous les albums associés
					 		à celle-ci. Vous pouvez ajouter au panier n'importe quel album contenant
					 		des enregistrements si vous êtes connecté. En cliquant sur la pochette d'un album
					 		vous aurez accès aux mêmes informations que précédemment
					 	</li>
					 </ul>
					L'ajout d'un album ne comportant aucun enregistrement affichera une erreur.
				</li>
				<li> Panier d'achat affichant, pour chaque album, son nom, les morceaux qui le composent et un lien vers ses détails.
				     Vous pouvez supprimer les articles du panier
				</li>
				<li> Zone sécurisée
					<ul>
						<li><i> Mode non connecté :</i> accès à toutes les pages sauf au panier 
							(l'utilisateur doit se connecter s'il veut ajouter un album au panier)
						</li>
						<li><i> Mode connecté :</i> accès à toutes les pages et possibilité d'ajouter
							des produits dans le panier, ainsi que d'accéder directement au panier
						</li>
					</ul>
				</li>	

				<li> Panier d'achat affichant le nom, le prix et les morceaux composant les albums du panier.
				     Vous pouvez supprimer les articles dans le panier
				</li>
				<li> Zone sécurisé
					<ul>
						<li><i> Mode non connecté :</i> accès à toutes les pages sauf le panier 
							(message d'erreur si l'utilisateur tente d'accéder au panier)
							, pas de panier </li>
						<li><i> Mode connecté :</i> accès à toutes les pages et possibilité d'ajouter
							des produits dans le panier </li>
					</ul>
				</li>
				
			</ul>

			<h3> Outils utilisés </h3>
			<ul>
				<li> HTML/CSS </li>
				<li> PHP/JS </li>
				<li> Bootstrap </li>
				<li> MS SQL </li>
			</ul>

			<h3> Auteurs du site </h3>
			<ul>
				<li> Maxime Guilbault </li>
				<li> Nicolas Sentout </li>
			</ul>
		</div>

	</body>
</html>
