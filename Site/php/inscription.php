<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title> Boutique Classique_Web </title>
        <link href="../css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/style.css" rel="stylesheet" type="text/css" media ="all" />
        <script src="../js/jquery.js"></script>
        <script src="../js/bootstrap.min.js"></script>
    </head>
    <body>

         <?php
            include("menu.php");
         ?>

        <!-- Formulaire de l'inscription -->
        <div class ="col-md-3">
            <form method="post" action="traiteInscription.php">
                Nom
                <div class="input-group">
                    <input type="text" name="Nom" class="form-control" placeholder="Nom">
                </div><br>
                Prénom
                <div class="input-group">
                    <input type="text" name="Prenom" class="form-control" placeholder="Prénom">
                </div><br>
                Identifiant
                <div class="input-group">
                    <input type="text" name="Login" class="form-control" placeholder="Identifiant">
                </div><br>
                Mot de passe
                <div class="input-group">
                    <input type="password" name="Password" class="form-control" placeholder="Mot de passe">
                </div><br>
                <input name="Post" type="submit" value="S'inscrire" />
            </form>
        </div>


    </body>
</html>