Projet réalisé dans le cadre de l'obtention du DUT informatique à l'IUT 
informatique de Bordeaux.
C'est une boutique sous forme de site web permettant d'acheter des albums de
musique via Amazon. Le site fournit une zone de recherche permettant de trouver
l'album que l'on cherche via son nom ou son compositeur et de l'ajouter à son
panier. Ce site a donc un système d'authentification et un panier d'achat 
affichant différentes caractéristiques de l'album.

Le site a été développé à l'aide des langages HTML, CSS, PHP, JavaScript et
MS SQL. Il utilise également le framework Bootstrap pour le design.

Il a été réalisé en binôme par Maxime GUILBAULT et Nicolas SENTOUT.